from bpy.props import EnumProperty, StringProperty
from bpy.types import Operator, Panel, PropertyGroup
import bpy

from . import xcp_data
class XCP_PT_Material(Panel):
    bl_idname = "XCP_PT_material"
    bl_label = "XCP Material Panel"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_options = {'DEFAULT_CLOSED'}
    bl_category = "XCP"

    @classmethod
    def poll(self, context):
        return context.object is not None

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        tool = scene.xcptool

        box = layout.box()
        col = box.column(align=True)
        col.label(text="Material Editor")
        col.prop(tool, "material_choice")
        col.operator("xcp.material")
        
        box = layout.box()
        col = box.column(align=True)
        col.label(text="Material Recorder")
        col.prop(tool, "material_record")
        col.operator("xcp.record")

class XCP_OT_MaterialUpdate(Operator):
    bl_label = "Update"
    bl_idname = "xcp.material"

    def init_node(self, data, key, color, nodes, links, nodedict):
        if key in nodedict:
            return
        node_type = key.split('.')[0]
        node_name = "ShaderNode{}".format(node_type)
        node_data = data[key]
        node = nodes.new(node_name)
        nodedict[key] = node
        # if the node is some kind of shader, set the default color as the old color
        # this color might be overwritten depending on the material
        if node_type in xcp_data.color_entries:
            print(xcp_data.color_entries[node_type], color)
            node.inputs[xcp_data.color_entries[node_type]].default_value = color
        # set all the defaults
        if 'inputs' in node_data:
            for i, d in enumerate(node_data['inputs']):
                if d == None:
                    # do nothing if None (blender defaults)
                    continue
                if type(d) is xcp_data.Link:
                    # try to form a link (it's ok if this fails as the other connected node should
                    # try to connect back)
                    if d.node not in nodedict:
                        # recursively call this link before proceeding
                        self.init_node(data, d.node, color, nodes, links, nodedict)
                    links.append((node.inputs[i], nodedict[d.node].outputs[d.target_index]))
                # otherwise try to just set the value
                else:
                    node.inputs[i].default_value = d
        for key in node_data:
            if key == 'inputs':
                continue
            if key == 'color_ramp':
                # this is a bit more complicated
                node.color_ramp.color_mode = node_data[key]["color_mode"]
                node.color_ramp.interpolation = node_data[key]["interpolation"]
                node.color_ramp.elements.remove(node.color_ramp.elements[1])
                node.color_ramp.elements.remove(node.color_ramp.elements[0])
                for i, e in enumerate(node_data[key]["elements"]):
                    node.color_ramp.elements.new(0.0)
                    node.color_ramp.elements[i].position = e[0]
                    node.color_ramp.elements[i].color = e[1]

            setattr(node, key, node_data[key])


    def execute(self, context):
        # TODO consider making all the materials on startup and then cloning and setting specific colors etc. 

        scene = context.scene
        tool = scene.xcptool

        # print the values to the console
        select_list = bpy.context.selected_objects
        material_list = []
        for s in select_list:
            for m in s.data.materials:
                material_list.append(m)
        print(material_list)

        for m in material_list:
            color = None
            # check if the pre-existing material setup aligns with the expected 
            if not m.use_nodes:
                # if not, try to get the color and set `use_nodes` to true, to be reset later
                color = m.diffuse_color
                m.use_nodes = True

            nodes = m.node_tree.nodes
            links = m.node_tree.links

            # if default setup, get the color from the principled bsdf, otherwise (for now)
            # complain and continue with no color
            # TODO sort out colors in non-standard cases
            # TODO maybe the atom color needs to be set less ambigiously?
            if "Principled BSDF" in nodes:
                color = nodes["Principled BSDF"].inputs[0].default_value[:4]
            if color is None:
                print("Unable to figure out the initial color for {}, setting to a default for the user to change".format(m))
                color = (0., 0., 0.)

            # recursively clear the existing `node_tree`
            links.clear()
            nodes.clear()

            # read in the data and build the tree
            material_data = xcp_data.get_material(tool.material_choice)
            # easy referencing for this function only
            new_node_dict = {}
            # store the links and add them later once all the nodes have been generated
            temp_links = []

            # init all the nodes (currently unconnected)
            for node_key in material_data:
                self.init_node(material_data, node_key, color, nodes, temp_links, new_node_dict)

            # set links for everything
            print("links", temp_links)
            for link in temp_links:
                links.new(link[0], link[1])

        return {'FINISHED'}

class XCP_OT_MaterialRecord(Operator):
    bl_label = "Record"
    bl_idname = "xcp.record"

    def execute(self, context):
        scene = context.scene
        tool = scene.xcptool

        # for now, let's not update previously existing material names, but instead increment
        # expect a single word 
        # TODO if not, try to parse it correctly
        new_name = tool.material_record.strip()
        stored_materials = xcp_data.get_labels()

        for label in stored_materials:
            pass

        return {'FINISHED'}

class PanelProperties(PropertyGroup):
    material_choice: EnumProperty(
        name="Name",
        description="Select a predefined material for the object",
        items=[
            ('mat.glossy', "Glossy", ""),
            ('mat.opaque', "Opaque", ""),
            ('mat.metallic', "Metal", ""),
            ('mat.glass.1', "Glass 1", ""),
            ('mat.glass.2', "Glass 2", ""),
            ('mat.glass.3', "Glass 3", ""),
            ('mat.glass.4', "Glass 4", ""),
            ('mat.glass.5', "Glass 5", ""),
            ('mat.translucent', "Translucent", ""),
            ('mat.plastic.1', "Plastic 1", ""),
            ('mat.plastic.2', "Plastic 2", ""),
            ('mat.chalk', "Chalk", ""),
            ('mat.sandstone', "Sandstone", ""),
            ('mat.granite', "Granite", ""),
        ],
    )

    material_record: StringProperty(
        name="Name",
        description="Name a custom material set by the user (needs to use nodes)",
        maxlen=64,
        default="",
    )
