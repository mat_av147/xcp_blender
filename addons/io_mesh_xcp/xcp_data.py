from dataclasses import dataclass
import pickle
import os

@dataclass
class Link:
    node: str
    target_index: int

color_entries = {
    'BsdfPrincipled': 0,
    'BsdfGlass': 0,
    'VolumeAbsorption': 0
}

material_data = {
    'mat.glossy': {
        'OutputMaterial.0': {
            'inputs': (
                Link('BsdfPrincipled.0', 0), None, None,
            )
        },
        'BsdfPrincipled.0': {
            'inputs': (
                None, None, None, None, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, (0.0, 0.0, 0.0, 1.0), 1.0, None, None, None,
            )
        }
    },
    'mat.opaque': {
        'OutputMaterial.0': {
            'inputs': (
                Link('BsdfPrincipled.0', 0), None, None,
            )
        },
        'BsdfPrincipled.0': {
            'inputs': (
                None, None, None, None, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, (0.0, 0.0, 0.0, 1.0), 1.0, None, None, None,
            )
        }
    },
    'mat.metallic': {
        'OutputMaterial.0': {
            'inputs': (
                Link('BsdfPrincipled.0', 0), None, None,
            )
        },
        'BsdfPrincipled.0': {
            'inputs': (
                None, None, None, None, 0.4, 0.0, 0.0, 0.3, 0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, (0.0, 0.0, 0.0, 1.0), 1.0, None, None, None,
            )
        }
    },
    'mat.glass.1': {
        'OutputMaterial.0': {
            'inputs': (
                Link('BsdfPrincipled.0', 0), None, None,
            )
        },
        'BsdfPrincipled.0': {
            'inputs': (
                None, None, None, None, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.1, 1.0, 0.0, (0.0, 0.0, 0.0, 1.0), 1.0, None, None, None,
            )
        }
    },
    'mat.translucent': {
        'OutputMaterial.0': {
            'inputs': (
                Link('BsdfPrincipled.0', 0), None, None,
            )
        },
        'BsdfPrincipled.0': {
            'inputs': (
                None, None, None, None, 0.0, 0.0, 0.0, 0.4, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 1.1, 0.9, 0.0, (0.0, 0.0, 0.0, 1.0), 1.0, None, None, None,
            )
        }
    },
    'mat.plastic.1': {
        'OutputMaterial.0': {
            'inputs': (
                Link('BsdfPrincipled.0', 0), None, None,
            )
        },
        'BsdfPrincipled.0': {
            'inputs': (
                None, None, None, None, 0.0, 0.1, 0.0, 0.2, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, (0.0, 0.0, 0.0, 1.0), 1.0, None, None, None,
            )
        }
    },
    'mat.plastic.2': {
        'OutputMaterial.0': {
            'inputs': (
                Link('BsdfPrincipled.0', 0), None, None,
            )
        },
        'BsdfPrincipled.0': {
            'inputs': (
                None, None, None, None, 0.0, 0.1, 0.0, 0.3, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, (0.0, 0.0, 0.0, 1.0), 1.0, None, None, None,
            )
        }
    },
    'mat.chalk': {
        'OutputMaterial.0': {
            'inputs': (
                Link('BsdfPrincipled.0', 0), None, None,
            )
        },
        'BsdfPrincipled.0': {
            'inputs': (
                None, None, (1.0, 0.2, 0.1), None, 0.0, 0.5, 0.0, 0.5, 0.0, 0.0,
                0.0, 0.5, 0.0, 0.03, 1.45, 0.0, 0.0, (0.0, 0.0, 0.0, 1.0), 1.0, Link('Bump.0', 0), None, None,
            )
        },
        'Bump.0': {
            'invert': True,
            'inputs': (
                0.2, 1.0, Link('TexNoise.0', 1),
            )
        },
        'TexNoise.0': {
            'noise_dimensions': '3D',
            'inputs': (
                None, None, 1.0, 20.0, 0.9, 0.0,
            )
        }
    },
    'mat.sandstone': {
        'OutputMaterial.0': {
            'inputs': (
                Link('BsdfPrincipled.0', 0), None, None,
            )
        },
        'BsdfPrincipled.0': {
            'inputs': (
                None, None, (1.0, 0.2, 0.1), None, 0.0, 0.5, 0.0, 0.5, 0.0, 0.0,
                0.0, 0.5, 0.0, 0.03, 1.45, 0.0, 0.0, (0.0, 0.0, 0.0, 1.0), 1.0, Link('Bump.0', 0), None, None,
            )
        },
        'Bump.0': {
            'invert': True,
            'inputs': (
                0.5, 1.0, Link('TexNoise.0', 1),
            )
        },
        'TexNoise.0': {
            'noise_dimensions': '3D',
            'inputs': (
                None, None, 1.0, 15.0, 10.0, 0.0,
            )
        }
    },
    'mat.granite': {
        'OutputMaterial.0': {
            'inputs': (
                Link('BsdfPrincipled.0', 0), None, None,
            )
        },
        'BsdfPrincipled.0': {
            'inputs': (
                None, None, (1.0, 0.2, 0.1), None, 0.0, 0.5, 0.0, 0.5, 0.0, 0.0,
                0.0, 0.5, 0.0, 0.03, 1.45, 0.0, 0.0, (0.0, 0.0, 0.0, 1.0), 1.0, Link('Bump.0', 0), None, None,
            )
        },
        'Bump.0': {
            'invert': True,
            'inputs': (
                0.4, 1.0, Link('TexNoise.0', 1),
            )
        },
        'TexNoise.0': {
            'noise_dimensions': '3D',
            'inputs': (
                None, None, 2.0, 10.0, 0.7, 0.0,
            )
        }
    },
    'mat.glass.2': {
        'OutputMaterial.0': {
            'inputs': (
                Link('BsdfGlass.0', 0), Link('VolumeAbsorption', 0), None,
            )
        },
        'BsdfGlass.0': {
            'distribution': "BECKMANN",
            'inputs': (
                None, 0.0, 1.45, None,
            )
        },
        'VolumeAbsorption': {
            'inputs': (
                None, 1.7,
            )
        }
    },
    'mat.glass.3': {
        'OutputMaterial.0': {
            'inputs': (
                Link('BsdfGlass.0', 0), Link('VolumeAbsorption', 0), None,
            )
        },
        'BsdfGlass.0': {
            'distribution': "BECKMANN",
            'inputs': (
                None, 0.2, 1.45, None,
            )
        },
        'VolumeAbsorption': {
            'inputs': (
                None, 1.2,
            )
        }
    },
    'mat.glass.4': {
        'OutputMaterial.0': {
            'inputs': (
                Link('BsdfGlass.0', 0), Link('VolumeAbsorption', 0), None,
            )
        },
        'MixShader.0': {
            'inputs': (
                Link('Math.0', 0), Link('BsdfGlass.0', 0), Link('BsdfTransparent.0', 0),
            )
        },
        'Math.0': {
            'operation': "Greater Than",
            'use_clamp': False,
            'inputs': (
                Link('LightPath.0', 8), 3.0,
            )
        },
        'LightPath.0': {},
        'BsdfGlass.0': {
            'distribution': "MULTI_GGX",
            'inputs': (
                None, 0.0, 1.45, None,
            )
        },
        'BsdfTransparent.0': {},
    },
    'mat.glass.5': {
        'OutputMaterial.0': {
            'inputs': (
                Link('AddShader.0', 0), None, None,
            ),
        },
        'AddShader.0': {
            'inputs': (
                Link('BsdfRefraction.0', 0), Link('AddShader.1', 0),
            )
        },
        'AddShader.1': {
            'inputs': (
                Link('BsdfRefraction.1', 0), Link('BsdfRefraction.2', 0),
            )
        },
        'BsdfRefraction.0': {
            'distribution': "BECKMANN",
            'inputs': (
                (1.0, 0.0, 0.0, 1.0), Link('ValToRGB.0', 0), 1.4, Link('Bump.0', 0),
            )
        },
        'BsdfRefraction.1': {
            'distribution': "BECKMANN",
            'inputs': (
                (0.0, 1.0, 0.0, 1.0), Link('ValToRGB.0', 0), 1.45, Link('Bump.0', 0),
            )
        },
        'BsdfRefraction.2': {
            'distribution': "BECKMANN",
            'inputs': (
                (0.0, 0.0, 1.0, 1.0), Link('ValToRGB.0', 0), 1.5, Link('Bump.0', 0),
            )
        },
        'Bump.0': {
            'invert': False,
            'inputs': (
                0.032, 1.0, Link('ValToRGB.1', 0), None,
            ),
        },
        'ValToRGB.0': {
            'color_ramp': {
                'color_mode': "RGB",
                'interpolation': "B-Spline",
                'elements': [
                    (0.61, (0.0, 0.0, 0.0, 1.0)),
                    (1.00, (1.0, 1.0, 1.0, 1.0))
                ]
            },
            'inputs': (
                Link('TexNoise.0', 1),
            )
        },
        'ValToRGB.1': {
            'color_ramp': {
                'color_mode': "RGB",
                'interpolation': "Linear",
                'elements': [
                    (0.44, (0.0, 0.0, 0.0, 1.0)),
                    (1.00, (1.0, 1.0, 1.0, 1.0))
                ]
            },
            'inputs': (
                Link('TexNoise.0', 1),
            )
        },
        'TexNoise.0': {
            'noise_dimensions': "3D",
            'inputs': (
                None, 0.9, 16.0, 0.1, 7.0,
            )
        }
    }
}

pickle_name = "materials.pickle"


def get_material(mat):
    # if not os.path.exists(pickle_name):
    #     # create one from the defaults stored in xcp_data
    #     pickle_defaults()

    # data = pickle.load(pickle_name)
    data = material_data
    return data[mat]

def get_labels():
    # if not os.path.exists(pickle_name):
    #     # create one from the defaults stored in xcp_data
    #     pickle_defaults()

    # data = pickle.load(pickle_name)
    data = material_data
    return list(data.keys())


def pickle_defaults(): 
    pickle.dump(material_data, pickle_name)           
